﻿#include <iostream>
#include <Windows.h>

#include "Planet.h"
#include "Galactic.h"

using namespace std;

int main()
{
	setlocale(LC_ALL, "rus");

	HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hOut, FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_INTENSITY);

	SetConsoleTitle(L"Ну программа");
	system("cls");


	Planet Earth(10, 3, 1);
	Earth.SetMass(100);
	Earth.SetRadius(2);

	Planet Mars(20, 13, 2);
	Mars.SetMass(100);
	Mars.SetRadius(2);

	Planet Venera(1, 2, -4);
	Venera.SetMass(100);
	Venera.SetRadius(2);

	//Mars.GetInfo();

	Earth.GetInfo();
	Earth.Move(25, 25, 50);
	Earth.SetRadius(5);
	Earth.GetInfo();

	//system("pause");

	Galactic Milky;
	Milky.Clear();

	Milky.AddPlanet(Earth);
	Milky.AddPlanet(Mars);
	Milky.AddPlanet(Venera);

	Milky.GalaxyInfo();

	system("pause");
	Milky.Clear();

	Milky.ShowThePlanet(1);

	return 0;
}