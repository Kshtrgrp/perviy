#pragma once
#include "Planet.h"

class Galactic
{
	Planet* Galaxy = new Planet[0];
	int CountOfPlanets = 0;

public:
	Galactic();
	~Galactic();

	void Clear();

	void AddPlanet(Planet name);
	void GalaxyInfo();
	int GetSize() { return CountOfPlanets; };
	void ShowThePlanet(int a);
	void DrawPlanet(int R, int x, int y);
};