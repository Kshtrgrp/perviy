#pragma once
#include <string>

using namespace std;

class Planet
{
	int x;
	int y;
	int z;
	int R = 0;
	int Mass = 0;
public:
	Planet();
	Planet(int x, int y, int z);

	void Move(int x, int y, int z);
	int GetX() { return x; };
	int GetY() { return y; };
	int GetZ() { return z; };

	int GetRadius() { return R; };
	int GetMass() { return Mass; };

	void SetRadius(int R);
	void SetMass(int Mass);

	void GetInfo();

	~Planet();
};